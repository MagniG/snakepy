import pygame as pg
from settings import *
import random


class Fruit(pg.sprite.Sprite):
    def __init__(self, game, x, y):
        self.groups = game.all_sprites
        pg.sprite.Sprite.__init__(self, self.groups)
        self.game = game
        self.image = pg.Surface((TILESIZE, TILESIZE))
        self.image.fill(RED)
        self.rect = self.image.get_rect()
        self.x = x
        self.y = y

    def collide(self, snake):
        head = snake.body[0]
        if (head.x, head.y) == (self.x, self.y):
            self.groups.remove(self)
            snake.eat()
            self.random()
            self.game.speed -= 1

    def random(self):
        x = random.randint(0, GRIDWIDTH)
        y = random.randint(0, GRIDHEIGHT)
        Fruit(self.game, x, y)
        print(x, y)

    def update(self):
        self.rect.x = self.x * TILESIZE
        self.rect.y = self.y * TILESIZE
        self.collide(self.game.snake)