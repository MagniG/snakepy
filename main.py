from settings import *
from snake import Snake, SnakePart
import pygame as pg
import sys
from items import Fruit


class Game:
    def __init__(self):
        pg.init()
        self.screen = pg.display.set_mode((WIDTH, HEIGHT))
        pg.display.set_caption(TITLE)
        self.clock = pg.time.Clock()
        pg.key.set_repeat(500, 100)
        self.load_data()
        self.speed = 150

    def load_data(self):
        pass

    def new(self):
        """Initialize all variables and do all setup for a new game"""
        self.all_sprites = pg.sprite.Group()
        self.walls  = pg.sprite.Group()
        self.snake = Snake(self, 5)
        self.fruit = Fruit(self, 5, 5)

    def run(self):
        self.playing = True
        while self.playing:
            self.dt = self.clock.tick(FPS) / 1000
            self.events()
            self.update()
            self.draw()

    def quit(self):
        pg.quit()
        sys.exit()

    def update(self):
        self.snake.update()
        self.all_sprites.update()

        pg.time.delay(self.speed)

    def draw_grid(self):
        for x in range(0, WIDTH, TILESIZE):
            pg.draw.line(self.screen, LIGHTGREY, (x, 0), (x, HEIGHT))
        for y in range(0, HEIGHT, TILESIZE):
            pg.draw.line(self.screen, LIGHTGREY, (0, y), (WIDTH, y))

    def draw(self):
        self.screen.fill(BGCOLOR)
        self.draw_grid()
        self.all_sprites.draw(self.screen)
        pg.display.flip()

    def events(self):
        for event in pg.event.get():
            if event.type == pg.QUIT:
                self.quit()
            if event.type == pg.KEYDOWN:
                if event.key == pg.K_LEFT:
                    self.snake.key('LEFT')
                if event.key == pg.K_RIGHT:
                    self.snake.key('RIGHT')
                if event.key == pg.K_UP:
                    self.snake.key('UP')
                if event.key == pg.K_DOWN:
                    self.snake.key('DOWN')

    def show_start_screen(self):
        pass

    def show_go_screen(self):
        pass

g = Game()
g.show_start_screen()
while True:
    g.new()
    g.run()
    g.show_go_screen()
