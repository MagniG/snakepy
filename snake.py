import pygame as pg
from settings import *


class SnakePart(pg.sprite.Sprite):
    def __init__(self, game, x, y):
        self.groups = game.all_sprites
        pg.sprite.Sprite.__init__(self, self.groups)
        self.game = game
        self.image = pg.Surface((TILESIZE, TILESIZE))
        self.image.fill(YELLOW)
        self.rect = self.image.get_rect()
        self.x = x
        self.y = y
        self.move()

    def move(self):
        if self.x < 0:
            self.x = GRIDWIDTH
        elif self.x > GRIDWIDTH:
            self.x = 0
        if self.y < 0:
            self.y = GRIDHEIGHT
        elif self.y > GRIDHEIGHT:
            self.y = 0

    def update(self):
        self.rect.x = self.x * TILESIZE
        self.rect.y = self.y * TILESIZE


class Snake:
    def __init__(self, game, length):
        self.body = []
        self.length = length
        self.game = game
        self.direction = 'RIGHT'

        for x in reversed(range(self.length)):
            self.add_part(x, 10)

        self.x_change = 1
        self.y_change = 0

    def add_part(self, x, y):
        part = SnakePart(self.game, x, y)
        self.body.append(part)
        self.game.all_sprites.add(part)

    def key(self, direction):
        if direction == 'LEFT' and self.direction != 'RIGHT':
            self.direction = 'LEFT'
            self.x_change = -1
            self.y_change = 0

        if direction == 'RIGHT' and self.direction != 'LEFT':
            self.direction = 'RIGHT'
            self.x_change = 1
            self.y_change = 0

        if direction == 'UP' and self.direction != 'DOWN':
            self.direction = 'UP'
            self.y_change = -1
            self.x_change = 0

        if direction == 'DOWN' and self.direction != 'UP':
            self.direction = 'DOWN'
            self.y_change = 1
            self.x_change = 0

    def update(self):
        last = self.body.pop()
        self.game.all_sprites.remove(last)
        x = self.body[0].x + self.x_change
        y = self.body[0].y + self.y_change
        first = SnakePart(self.game, x, y)

        self.body.insert(0, first)
        self.game.all_sprites.add(first)
        self.collide()

    def eat(self):
        x = self.body[-1].x + self.x_change
        y = self.body[-1].y + self.y_change
        part = SnakePart(self.game, x, y)

        self.body.append(part)
        self.game.all_sprites.add(part)

    def collide(self):
        body = self.body.copy()
        body.pop(0)
        for part in body:
            if (self.body[0].x, self.body[0].y) == (part.x, part.y):
                self.game.playing = False
